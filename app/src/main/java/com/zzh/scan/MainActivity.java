package com.zzh.scan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.client.android.Intents;
import com.google.zxing.client.android.PreferencesActivity;
import com.google.zxing.client.android.encode.EncodeActivity;
import com.zzh.lib.qr.HScanCode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.tv_scan).setOnClickListener(view -> {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                HScanCode.startHMSActivityForResult(MainActivity.this, 2000);
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA,}, 1000);
            }
        });

        findViewById(R.id.btn_property).setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, PreferencesActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.btn_encode).setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, EncodeActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.btn_hms_scan_code).setOnClickListener(v -> {
            HScanCode.startHMSActivityForResult(MainActivity.this, 2000);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 2000) {
                HScanCode.handleHMSScanResult(data);

            } else {
                if (data.hasExtra(Intents.Scan.RESULT)) {
                    Toast.makeText(this, data.getStringExtra(Intents.Scan.RESULT), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "没有返回的数据", Toast.LENGTH_LONG).show();
                }
            }


        } else {
            Toast.makeText(this, "返回数据data为空", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}