package com.google.zxing.client.android.callback;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;
import com.google.zxing.client.android.ViewfinderView;
import com.google.zxing.client.android.camera.CameraManager;

/**
 * Created by ZZH on 2020/11/24.
 *
 * @Date: 2020/11/24
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 扫描处理通用接口
 */
public interface ScanResultInterface {

    /**
     * 处理扫描结果
     *
     * @param rawResult   原始数据
     * @param barcode     扫描
     * @param scaleFactor 因子
     */
    void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor);

    void handleDecode(final String result);

    void drawViewfinder();

    ViewfinderView getViewfinderView();

    CameraManager getCameraManager();

    Handler getHandler();

    Activity getExtContext();

    void restartPreviewAfterDelay(long delay);
}
