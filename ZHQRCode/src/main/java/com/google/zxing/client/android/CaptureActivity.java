
package com.google.zxing.client.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.google.zxing.client.android.callback.ScanResultCallback;
import com.google.zxing.client.android.frag.CaptureFragment;
import com.zzh.lib.qr.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

/**
 * 二维码扫描
 */
public class CaptureActivity extends FragmentActivity implements ScanResultCallback {
    CaptureFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!showTitleBar()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        setContentView(R.layout.act_capture);
        fragment = (CaptureFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        fragment.setScanResultCallback(this);
    }

    protected boolean showTitleBar() {
        return false;
    }

    @Override
    public void onScanResult(String result) {
        Intent data = new Intent();
        data.putExtra(Intents.Scan.RESULT, result);
        setResult(RESULT_OK, data);
        finish();
    }
}
