package com.google.zxing.client.android.callback;

/**
 * Created by ZZH on 2020/11/24.
 *
 * @Date: 2020/11/24
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 扫描结果回调通知
 */
public interface ScanResultCallback {
    /**
     * 扫描结果回调
     *
     * @param result 扫描出的数据值
     */
    public void onScanResult(String result);
}
