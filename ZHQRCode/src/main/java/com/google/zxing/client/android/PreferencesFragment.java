/*
 * Copyright (C) 2013 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.client.android;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;

import com.zzh.lib.qr.R;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Implements support for barcode scanning preferences.
 *
 * @see PreferencesActivity
 */
public final class PreferencesFragment
        extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

//  private CheckBoxPreference[] checkBoxPrefs;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.preferences);

        PreferenceScreen preferences = getPreferenceScreen();
        preferences.getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
//    checkBoxPrefs = findDecodePrefs(preferences,
//                                    PreferencesActivity.KEY_DECODE_1D_PRODUCT,
//                                    PreferencesActivity.KEY_DECODE_1D_INDUSTRIAL,
//                                    PreferencesActivity.KEY_DECODE_QR,
//                                    PreferencesActivity.KEY_DECODE_DATA_MATRIX,
//                                    PreferencesActivity.KEY_DECODE_AZTEC,
//                                    PreferencesActivity.KEY_DECODE_PDF417);
//    disableLastCheckedPref();
        ((PreferenceGroup) findPreference("preferences_when_find")).removePreference(findPreference(PreferencesActivity.KEY_COPY_TO_CLIPBOARD));
        ((PreferenceGroup) findPreference("preferences_when_find")).removePreference(findPreference(PreferencesActivity.KEY_AUTO_OPEN_WEB));
        ((PreferenceGroup) findPreference("preferences_when_find")).removePreference(findPreference(PreferencesActivity.KEY_REMEMBER_DUPLICATES));
        ((PreferenceGroup) findPreference("preferences_when_find")).removePreference(findPreference(PreferencesActivity.KEY_ENABLE_HISTORY));
        ((PreferenceGroup) findPreference("preferences_when_find")).removePreference(findPreference(PreferencesActivity.KEY_SUPPLEMENTAL));

        ((PreferenceGroup) findPreference("preferences_general")).removePreference(findPreference(PreferencesActivity.KEY_BULK_MODE));
        ((PreferenceGroup) findPreference("preferences_general")).removePreference(findPreference(PreferencesActivity.KEY_DISABLE_AUTO_ORIENTATION));
        ((PreferenceGroup) findPreference("preferences_general")).removePreference(findPreference(PreferencesActivity.KEY_INVERT_SCAN));
        ((PreferenceGroup) findPreference("preferences_equipment_adaptation")).removePreference(findPreference(PreferencesActivity.KEY_DISABLE_EXPOSURE));
        ((PreferenceGroup) findPreference("preferences_equipment_adaptation")).removePreference(findPreference(PreferencesActivity.KEY_DISABLE_METERING));
        ((PreferenceGroup) findPreference("preferences_equipment_adaptation")).removePreference(findPreference(PreferencesActivity.KEY_DISABLE_BARCODE_SCENE_MODE));
        getPreferenceScreen().removePreference(findPreference("preferences_result"));
        ((CheckBoxPreference) preferences.findPreference(PreferencesActivity.KEY_DECODE_1D_INDUSTRIAL)).setChecked(true);
        ((CheckBoxPreference) preferences.findPreference(PreferencesActivity.KEY_DECODE_QR)).setChecked(true);
        ((CheckBoxPreference) preferences.findPreference(PreferencesActivity.KEY_AUTO_FOCUS)).setChecked(true);
        preferences.findPreference(PreferencesActivity.KEY_DECODE_1D_INDUSTRIAL).setEnabled(false);
        preferences.findPreference(PreferencesActivity.KEY_DECODE_QR).setEnabled(false);
        preferences.findPreference(PreferencesActivity.KEY_AUTO_FOCUS).setEnabled(false);
//        preferences.findPreference(PreferencesActivity.KEY_DISABLE_CONTINUOUS_FOCUS).setEnabled(false);

    }

    private static CheckBoxPreference[] findDecodePrefs(PreferenceScreen preferences, String... keys) {
        CheckBoxPreference[] prefs = new CheckBoxPreference[keys.length];
        for (int i = 0; i < keys.length; i++) {
            prefs[i] = (CheckBoxPreference) preferences.findPreference(keys[i]);
        }
        return prefs;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//    disableLastCheckedPref();
    }

//  private void disableLastCheckedPref() {
//    Collection<CheckBoxPreference> checked = new ArrayList<>(checkBoxPrefs.length);
//    for (CheckBoxPreference pref : checkBoxPrefs) {
//      if (pref.isChecked()) {
//        checked.add(pref);
//      }
//    }
//    boolean disable = checked.size() <= 1;
//    for (CheckBoxPreference pref : checkBoxPrefs) {
//      pref.setEnabled(!(disable && checked.contains(pref)));
//    }
//  }

    private final class CustomSearchURLValidator implements Preference.OnPreferenceChangeListener {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (!isValid(newValue)) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(PreferencesFragment.this.getActivity());
                builder.setTitle(R.string.msg_error);
                builder.setMessage(R.string.msg_invalid_value);
                builder.setCancelable(true);
                builder.show();
                return false;
            }
            return true;
        }

        private boolean isValid(Object newValue) {
            // Allow empty/null value
            if (newValue == null) {
                return true;
            }
            String valueString = newValue.toString();
            if (valueString.isEmpty()) {
                return true;
            }
            // Before validating, remove custom placeholders, which will not
            // be considered valid parts of the URL in some locations:
            // Blank %t and %s:
            valueString = valueString.replaceAll("%[st]", "");
            // Blank %f but not if followed by digit or a-f as it may be a hex sequence
            valueString = valueString.replaceAll("%f(?![0-9a-f])", "");
            // Require a scheme otherwise:
            try {
                URI uri = new URI(valueString);
                return uri.getScheme() != null;
            } catch (URISyntaxException use) {
                return false;
            }
        }
    }

}
