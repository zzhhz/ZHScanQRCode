package com.zzh.lib.qr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.zxing.client.android.CaptureActivity;
import com.google.zxing.client.android.Intents;
import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.ml.scan.HmsScan;
import com.huawei.hms.ml.scan.HmsScanAnalyzerOptions;

import androidx.fragment.app.Fragment;

/**
 * Created by ZZH on 2020/10/23.
 *
 * @Date: 2020/10/23
 * @Email: zzh_hz@126.com
 * @QQ: 1299234582
 * @Author: zzh
 * @Description: 跳转方法
 */
public class HScanCode {

    public static void startActivity(Context ctx) {
        Intent intent = new Intent(ctx, CaptureActivity.class);
        ctx.startActivity(intent);
    }

    public static void startActivity(Fragment ctx) {
        Intent intent = new Intent(ctx.getActivity(), CaptureActivity.class);
        ctx.startActivity(intent);
    }

    /**
     * 打开扫描摄像头
     *
     * @param ctx         上下文
     * @param requestCode 请求值
     *                    返回值放到data.里面，com.google.zxing.client.android.Intents.Scan.RESULT值
     */
    public static void startActivityForResult(Activity ctx, int requestCode) {
        Intent intent = new Intent(ctx, CaptureActivity.class);
        ctx.startActivityForResult(intent, requestCode);
    }

    /**
     * 打开扫描摄像头
     *
     * @param ctx         上下文
     * @param requestCode 请求值
     *                    返回值放到data.里面，com.google.zxing.client.android.Intents.Scan.RESULT值
     */
    public static void startActivityForResult(Fragment ctx, int requestCode) {
        Intent intent = new Intent(ctx.getActivity(), CaptureActivity.class);
        ctx.startActivityForResult(intent, requestCode);
    }

    /**
     * 打开扫描摄像头
     *
     * @param ctx         上下文
     * @param requestCode 请求值
     *                    返回值放到data.里面，com.google.zxing.client.android.Intents.Scan.RESULT值
     * @param width       扫描框宽度
     * @param height      扫描框高度
     */
    public static void startActivityForResult(Activity ctx, int width, int height, int requestCode) {
        Intent intent = new Intent(ctx, CaptureActivity.class);
        intent.putExtra(Intents.Scan.WIDTH, width);
        intent.putExtra(Intents.Scan.HEIGHT, height);
        ctx.startActivityForResult(intent, requestCode);
    }

    /**
     * 打开扫描摄像头
     *
     * @param ctx         上下文
     * @param requestCode 请求值
     *                    返回值放到data.里面，com.google.zxing.client.android.Intents.Scan.RESULT值
     * @param width       扫描框宽度
     * @param height      扫描框高度
     */
    public static void startActivityForResult(Fragment ctx, int width, int height, int requestCode) {
        Intent intent = new Intent(ctx.getActivity(), CaptureActivity.class);
        intent.putExtra(Intents.Scan.WIDTH, width);
        intent.putExtra(Intents.Scan.HEIGHT, height);
        ctx.startActivityForResult(intent, requestCode);
    }

    /**
     * 打开扫描摄像头
     *
     * @param ctx         上下文
     * @param requestCode 请求值
     *                    返回值放到data.里面，com.google.zxing.client.android.Intents.Scan.RESULT值
     * @param width       扫描框宽度
     * @param height      扫描框高度
     */
    public static void startActivityForResult(Activity ctx, int width, int height, boolean showTitleBar, int requestCode) {
        Intent intent = new Intent(ctx, CaptureActivity.class);
        intent.putExtra(Intents.Scan.WIDTH, width);
        intent.putExtra(Intents.Scan.TITLE_BAR, showTitleBar);
        intent.putExtra(Intents.Scan.HEIGHT, height);
        ctx.startActivityForResult(intent, requestCode);
    }

    /**
     * 打开扫描摄像头
     *
     * @param ctx         上下文
     * @param requestCode 请求值
     *                    返回值放到data.里面，com.google.zxing.client.android.Intents.Scan.RESULT值
     * @param width       扫描框宽度
     * @param height      扫描框高度
     */
    public static void startActivityForResult(Fragment ctx, int width, int height, boolean showTitleBar, int requestCode) {
        Intent intent = new Intent(ctx.getActivity(), CaptureActivity.class);
        intent.putExtra(Intents.Scan.WIDTH, width);
        intent.putExtra(Intents.Scan.HEIGHT, height);
        intent.putExtra(Intents.Scan.TITLE_BAR, showTitleBar);
        ctx.startActivityForResult(intent, requestCode);
    }

    /**
     * 启动华为扫码服务
     *
     * @param ctx         上下文
     * @param requestCode 请求🐴
     */
    public static int startHMSActivityForResult(Fragment ctx, int requestCode) {
        HmsScanAnalyzerOptions options = new HmsScanAnalyzerOptions.Creator().create();
        return ScanUtil.startScan(ctx.getActivity(), requestCode, options);
    }

    public static int startHMSActivityForResult(Activity ctx, int requestCode) {
        HmsScanAnalyzerOptions options = new HmsScanAnalyzerOptions.Creator().create();
        return ScanUtil.startScan(ctx, requestCode, options);
    }

    /**
     * 处理华为返回的
     *
     * @param data
     * @return
     */
    public static String handleHMSScanResult(Intent data) {
        if (data == null) {
            return null;
        }
        HmsScan hmsScan = data.getParcelableExtra(ScanUtil.RESULT);
        Log.d("vae", hmsScan.showResult);
        if (hmsScan != null) {
            return hmsScan.showResult;
        }
        return null;
    }

    /**
     * 处理华为扫码返回值
     *
     * @param data
     * @return
     */
    public static HmsScan handleHMSScanResultModel(Intent data) {
        if (data == null) {
            return null;
        }
        return data.getParcelableExtra(ScanUtil.RESULT);
    }
}
